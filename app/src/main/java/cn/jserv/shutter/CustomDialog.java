package cn.jserv.shutter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class CustomDialog extends Dialog implements View.OnClickListener {

    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        getWindow().setBackgroundDrawableResource(R.drawable.dialog_background);
        setCancelable(false);
        totalEditText = findViewById(R.id.total);
        intervalEditText = findViewById(R.id.interval);
        bulbTimeEditText = findViewById(R.id.bulb_time);
        if (mode == 1) {
            intervalEditText.setVisibility(View.GONE);
            totalEditText.setVisibility(View.GONE);
        } else if (mode == 2) {
            bulbTimeEditText.setVisibility(View.GONE);
        }
        Button confirmBtn = findViewById(R.id.btn_ok);
        Button cancelBtn = findViewById(R.id.btn_cancel);
        confirmBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (null != confirmListener) {
                    confirmListener.onClick(v);
                }
                break;
            case R.id.btn_cancel:
                if (null != cancelListener) {
                    cancelListener.onClick(v);
                }
                break;
            default:
                break;
        }
    }

    public CustomDialog setConfirm(View.OnClickListener listener) {
        this.confirmListener = listener;
        return this;
    }

    public CustomDialog setCancel(View.OnClickListener listener) {
        this.cancelListener = listener;
        return this;
    }

    public CustomDialog setMode(byte mode) {
        this.mode = mode;
        return this;
    }

    private View.OnClickListener confirmListener, cancelListener;

    private EditText totalEditText, intervalEditText, bulbTimeEditText;

    private byte mode;

    public EditText getTotalEditText() {
        return totalEditText;
    }

    public EditText getIntervalEditText() {
        return intervalEditText;
    }

    public EditText getBulbTimeEditText() {
        return bulbTimeEditText;
    }
}
