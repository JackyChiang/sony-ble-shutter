package cn.jserv.shutter;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class DeviceListAdapter extends ArrayAdapter<BluetoothDevice> {

    public DeviceListAdapter(@NonNull Context context, int res, @NonNull List<BluetoothDevice> objects) {
        super(context, res, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        BluetoothDevice device = getItem(position);
        @SuppressLint("ViewHolder")
        View view = LayoutInflater.from(getContext()).inflate(R.layout.device_item, parent, false);
        TextView nameView = view.findViewById(R.id.device_name);
        TextView macView = view.findViewById(R.id.device_mac);
        nameView.setText(null == device.getName() ? "N/A" : device.getName());
        int bondState = device.getBondState();
        macView.setText(device.getAddress());
        return view;
    }
}
